RYZEN 3700X ASUS PRIM X570 EFI
------------------------------

1. Set Write flash to false
2. Added npci=0x2000 to boot args
3. Disabled NVIDIA gpu slot 0 in Device Properties
4. Added required CPU Patch for 8 core 16 thread AMD processor.
5. Added AppleMCEReporterDisabler.kext as 12.5 won't boot without it on AMD Cpus.

Notes: 

- No kexts for wifi is added as using BCM 94360CS2, which works natively.
- Using AppleALC and works without any issues, Added Layout 21 in boot args.
- All EFI and KEXT files are debug builds.

